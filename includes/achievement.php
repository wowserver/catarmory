<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Achievement extends Cache {

	protected $db;
	protected $id;

	private $_achievement;

	function __construct($db,$id) {
		$this->db = $db;
		$this->id = $id;

                // search for cached data. Set variable and stop processing when found.
		if ($this->_achievement = $this->get_cache(array('achievement',$id),GENERAL_DBC_EXPIRE)) {
			return;
		}

		$get_achievement = $this->db->query('
			SELECT da.`col_0` AS id,da.`col_1` AS faction,da.`col_2` AS map,da.`col_3` AS parent,da.`col_4` AS name,da.`col_5` AS description,da.`col_7` AS points,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon,da.`col_11` AS reward,da.`col_12` AS count
			FROM `dbc_achievement` AS da
			LEFT JOIN `dbc_spellicon` AS dsi ON (da.`col_10`=dsi.`col_0`)
			WHERE da.`col_0`=?',
			array($id)
		);
		if ($get_achievement->rowCount() == 1) {
			$this->_achievement = $get_achievement->fetch(PDO::FETCH_ASSOC);
			$this->store_cache(array('achievement',$id),$this->_achievement);
		}
	}


	/**
	 * Returns achievement informations
	 * @return array achievement informations
	 */
	public function get_achievement() {
		if (!$this->_achievement['id'])
			return;

		return $this->_achievement;
	}

	/**
	 * Returns achievement name
	 * @return string achievement name
	 */
	public function get_name() {
		return $this->_achievement['name'];
	}

	public function get_achievers() {
		// search for cached data. Set variable and stop processing when found.
		if ($achievers = $this->get_cache(array('achievers',$id),ACHIEVERS_EXPIRE)) {
			return $achievers;
		}

		$get_achievers = $this->db->query('
			SELECT ca.`guid`,ca.`date`
			FROM  `'.$this->db->characterdb.'`.`character_achievement` AS ca 
			WHERE ca.`achievement`=? ORDER BY ca.`date` DESC LIMIT '.SQL_LIMIT,
			array($this->id)
		);
		$achievers = array();
		foreach ($get_achievers->fetchAll(PDO::FETCH_ASSOC) as $a) {
			$char = new Character($this->db,$a['guid']);
			$achiever = $char->get_char();
			$achievers[] = array('date' => $a['date'], 'guid' => $achiever['guid'], 'name' => $achiever['name'],'race' => $achiever['race'], 'class' => $achiever['class'], 'level' => $achiever['level'], 'guildid' => $achiever['guildid'], 'guildName' => $achiever['guildName']);
		}
		$this->store_cache(array('achievers',$id),$achievers);
		return $achievers;
	}

}
