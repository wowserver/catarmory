<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Auth extends Cache {
	public $db;
	public $Armory;

	/**
	 * @param PDO database handler
	 */
	function __construct($db,$armory) {
		$this->db = $db;
		$this->Armory = $armory;
	}

	/**
	 * Create new challenge
	 * @return array Challenge string and challenge Id
	 */
	public function challenge($login) {
		$get_user = $this->db->query('
			SELECT id
			FROM `'.$this->db->realmdb.'`.`account`
			WHERE username=?',
			array($login)
		);

		if ($get_user->rowCount() == 1) {
			$account = $get_user->fetch(PDO::FETCH_ASSOC);
		} else {
			// unknown user - to avoid somebody will using this to check whether user account exists or not, we'll generate the challenge, but with account id = 0
			$account['id'] = 0;
		}
		$challenge = $this->_random(32);
		$this->db->query('INSERT INTO `challenges` (challenge,account,ip,stamp) VALUES (?,?,?,NOW())',array($challenge,$account['id'],getenv('REMOTE_ADDR')));
		$id = $this->db->lastId();
		return array('challenge' => $challenge, 'id' => $id);


	}
	
	/**
	 * Evaluates challenge response from client
	 * @return array/boolean characters on account or false
	 */
	public function response($challenge_id,$response) {
		// challenge lasts for 7 days - as it is usable for later logins
		$get_challenge = $this->db->query('
			SELECT `challenge`,`account`
			FROM `challenges`
			WHERE `id`=? AND `ip`=? AND DATE_ADD(`stamp`, INTERVAL 7 DAY) > NOW()',
			array($challenge_id,getenv('REMOTE_ADDR'))
		);

		// check if challenge exists and is generated for existing account
		if ($get_challenge->rowCount() == 1) {
			$challenge = $get_challenge->fetch(PDO::FETCH_ASSOC);
			if ($challenge['account'] != 0) {
				$get_account = $this->db->query('
					SELECT `username`,`sha_pass_hash`
					FROM `'.$this->db->realmdb.'`.`account`
					WHERE `id`=?',
					array($challenge['account'])
				);
				$account = $get_account->fetch(PDO::FETCH_ASSOC);
				$hash = strtoupper(hash('sha1',  strtoupper($account['sha_pass_hash']).':'.strtoupper($challenge['challenge']) ));

				// check if client response matches with server's
				if ($response == $hash) {
					$_SESSION['account'] = $challenge['account'];
					// good login - revitalize challenge to last now() + 7 days
					$update_stamp = $this->db->query('
						UPDATE `challenges`
						SET `stamp`=NOW()
						WHERE `id`=?',
						array($challenge_id)
					);
					// wait 1 seconds - to avoid bruteforce attacks. Attacker must use more concurrent threads and it will most probably disclose him
					sleep(1);

					return array( 'characters' => $this->_get_characters(), 'profile' => $this->_get_profile() );
				}
			}
		}
		// wait 1 seconds - to avoid bruteforce attacks. Attacker must use more concurrent threads and it will most probably disclose him
		sleep(1);

		// bad password, expired challenge, noexistand username or invalid ip. But we'll mask it as general username/password error
		$this->Armory->throw_error(401,"Method Error","Bad username or password");
		return false;
	}

	/**
	 * Checks the state and returns false when not logged in and list of characters when logged in
	 * @return array/boolean characters on account or false
	 */
	public function check() {
		if ($this->loggedState()) {
			return array( 'characters' => $this->_get_characters(), 'profile' => $this->_get_profile() );
		} else {
			if (array_key_exists('challenge',$_COOKIE) && array_key_exists('response',$_COOKIE)) {
				return $this->response($_COOKIE['challenge'],$_COOKIE['response']);
			}
			return FALSE;
		}
	}

	/**
	 * Perform logout
	 */
	public function logout() {
		$_SESSION['account'] = 0;
		session_destroy();
		return;
	}

	/**
	 * Returns whether session is logged or not
	 * @return boolean logged state
	 */
	public function loggedState() {
		return ((array_key_exists('account', $_SESSION) && $_SESSION['account'] > 0) ? TRUE : FALSE);
	}

	/**
	 * Returns the highest guild rank from user's characters in specified guild
	 * @param guildid Guild id to search for
	 * @return integer highest rank id
	 */

	public function allow_guild_access($guildid) {
		$rank = 0;
		foreach ($this->_get_characters() as $c) {
			if ($c['guildid'] == $guildid) {
				// return rank+1 (guildmaster rank is 0 and it is considered as FALSE...)
				if ($c['rank']+1 > $rank)
					$rank = $c['rank']+1;
			}
		}
		return $rank;
	}

	/**
	 * Returns characters that belongs to logged account
	 * @return array characters
	 */
	private function _get_characters() {

		if ($characters = $this->get_cache(array('account_characters',$_SESSION['account']),PROFILE_EXPIRE)) {
			return $characters;
		} else {
			$get_characters = $this->db->query('
				SELECT ch.`guid`,ch.`name`,ch.`race`,ch.`class`,ch.`level`,g.`guildid`,g.`rank`
				FROM `'.$this->db->characterdb.'`.`characters` AS ch
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_member` AS g ON (ch.`guid`=g.`guid`)
				WHERE ch.`account` = ?',
				array($_SESSION['account'])
			);
			$characters = $get_characters->fetchAll(PDO::FETCH_ASSOC);
			$this->store_cache(array('account_characters',$_SESSION['account']),$characters);

			return $characters;
		}
	}

	/**
	 * Returns profile data
	 * @return array profile
	 */
	private function _get_profile() {
		$get_profile = $this->db->query('
			SELECT `character_id`
			FROM `accounts`
			WHERE `id` = ?',
			array($_SESSION['account'])
		);
		if ($get_profile->rowCount() == 1) {
			return $get_profile->fetch(PDO::FETCH_ASSOC);
		} else {
			return array( 'character_id' => 0 );
		}
	}

	/**
	 * Set user's profile settings
	 */
	public function set_profile($character_id) {
		$this->db->query('REPLACE INTO `accounts` (id,character_id) VALUES (?,?)',array($_SESSION['account'],$character_id));
		return array( 'profile' => $this->_get_profile() );
	}

	/**
	 * Returns random string
	 * @param integer l Length of string
	 * @param string c (optional) Alphanumeric string to pick characters from
 	 * @return string Random string
	 */
	private function _random ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
		for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
		return $s;
	}

}
