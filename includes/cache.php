<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Cache {

	protected $cached;

	/**
	 * Returns false or cached data array.
	 * @param array request identifier. Returning false when cached data not found by this array.
	 * @param integer data expiration. Returning false when cached data are older than this.
	 * @param object (optional) Armory object - to control output.
	 * @return array|boolean array when cached data are found, false otherwise.
	 */
	public function get_cache($params,$expiration,$armory) {
		global $dir;
		$string = $this->_get_string($params);
		if (file_exists($dir['cache'].'/'.$string)) {						// if cache file exists
			$cached = filemtime($dir['cache'].'/'.$string);
			if (!$expiration || ($cached + $expiration > time())) {				// if file mtime + expiration is greater than actual unixtime or expiration is not set
				$json = file_get_contents($dir['cache'].'/'.$string);			// read data from file
				if (isset($armory))
					$armory->set_cached(array($cached,$expiration));
				return json_decode($json,true);
			}
		}
		return false;
	}

	/**
	 * Stores data as file on disk.
	 * @param array request identifier. Identifier is calculated by _get_string() method.
	 * @param array data to store as json serialized string.
	*/
	public function store_cache($params,$data) {
		global $dir;
		$string = $this->_get_string($params);
		file_put_contents($dir['cache'].'/'.$string,json_encode($data));		// serialize data and write them into file
	}

	/**
	 * Returns joined elements of array with underscore, converted to lowercase and base64 encoded
	 * @return string unique string of request
	 */
	private function _get_string($params) {
		return base64_encode(strtolower(implode('_',$params)));
	}
}

