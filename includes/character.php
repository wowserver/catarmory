<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class Character extends Cache {

	public $Armory;

	protected $char;
	protected $db;

	/**
	 * @param PDO database handler
	 * @param string|integer guid or name of character
	 */
	function __construct($db,$id,$armory) {
		$this->db = $db;
		$this->Armory = $armory;

		// search for cached data. Set variable and stop processing when found.
		if ($this->char = $this->get_cache(array('char',$id),CHAR_EXPIRE,$this->Armory)) {
			return;
		}

		if (intval($id) != 0) {
			$get_char = $this->db->query('
				SELECT ch.`guid`,ch.`account`,ch.`name`,ch.`race`,ch.`class`,ch.`level`,ch.`money`,ch.`health`,ch.`power1`,ch.`power2`,ch.`power3`,ch.`power4`,ch.`power5`,ch.`totalKills`,ch.`todayKills`,ch.`yesterdayKills`,chg.guildid,chg.name AS guildName,chgr.rname AS guildRank
				FROM `'.$this->db->characterdb.'`.`characters` AS ch
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_member` AS chgm ON (ch.`guid`=chgm.`guid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild` AS chg ON (chgm.`guildid`=chg.`guildid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_rank` AS chgr ON (chgm.`guildid`=chgr.`guildid` AND chgm.`rank`=chgr.`rid`)
				WHERE ch.`guid`=?',
				array($id)
			);
		} else if (is_string($id)) {
			$get_char = $this->db->query('
 				SELECT ch.`guid`,ch.`account`,ch.`name`,ch.`race`,ch.`class`,ch.`level`,ch.`money`,ch.`health`,ch.`power1`,ch.`power2`,ch.`power3`,ch.`power4`,ch.`power5`,ch.`totalKills`,ch.`todayKills`,ch.`yesterdayKills`,chg.guildid,chg.name AS guildName,chgr.rname AS guildRank
				FROM `'.$this->db->characterdb.'`.`characters` AS ch
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_member` AS chgm ON (ch.`guid`=chgm.`guid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild` AS chg ON (chgm.`guildid`=chg.`guildid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_rank` AS chgr ON (chgm.`guildid`=chgr.`guildid` AND chgm.`rank`=chgr.`rid`)
				WHERE ch.`name` COLLATE utf8_general_ci = ?',
				array($id)
			);
		} else {
			/// ???
		}

		if ($get_char && $get_char->rowCount() == 1) {
			$this->char = $get_char->fetch(PDO::FETCH_ASSOC);
			$this->store_cache(array('char',$id),$this->char);
		}
	}

	/**
	 * Returns character name
	 * @return string character name
	 */
	public function get_char() {
		if (!$this->char['guid'])
			return;

error_log("if (".$_SESSION['account']." != ".$this->char['account'].")");


		if ($_SESSION['account'] != $this->char['account']) 
			$this->char['money'] = NULL;

		return $this->char;
	}

	/**
	 * Returns character guid
	 * @return integer character guid
	 */
	public function get_guid() {
		if (!$this->char['guid'])
			return;

		return $this->char['guid'];
	}

	/**
	 * Gets character's inventory: Equipped items and items in bags and bank
	 * @return array character's inventory
	 */
	public function get_char_items() {
		if (!$this->char['guid'])
			return;

		if ($inventory = $this->get_cache(array('char_items',$this->char['guid']),CHAR_INVENTORY_EXPIRE,$this->Armory)) {
			return $inventory;
		}

		$get_inventory = $this->db->query('
			SELECT ci.`bag`,ci.`slot`,ci.`item` AS guid,ii.`itemEntry`,ii.`count`,dis.`col_99` AS name,dis.`col_1` AS quality,dis.`col_12` AS itemLevel,dis.`col_9` AS inventoryType,dis.col_23 AS ContainerSlots,LOWER(idi.`col_5`) AS icon
			FROM `'.$this->db->characterdb.'`.character_inventory AS ci
			LEFT JOIN `'.$this->db->characterdb.'`.item_instance AS ii ON (ci.`item`=ii.`guid`)
			LEFT JOIN `db2_item_sparse` AS dis ON (ii.`itemEntry`=dis.`col_0`)
			LEFT JOIN `db2_item` AS di ON (ii.`itemEntry`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE ci.`guid` = ?',
			array($this->char['guid'])
		);
		$this->items = $get_inventory->fetchAll(PDO::FETCH_ASSOC);
		$gearscore = $this->_get_char_gearscore();
		$inventory = array('items' => $this->items, 'gearscore' => $gearscore);
		
		$this->store_cache(array('char_items',$this->char['guid']),$inventory);
		return $inventory;
	}

	/**
	 * Gets character's learned skills (professions, secondary skills, riding, language, weapon skills, etc). 
	 * @return array character's skills
	 */
	public function get_char_skills() {
		if (!$this->char['guid'])
			return;

		if ($skills = $this->get_cache(array('char_skills',$this->char['guid']),CHAR_EXPIRE)) {
			return $skills;
		}

		$get_skills = $this->db->query('
			SELECT cs.`skill`,cs.`value`,cs.`max`,dsl.`col_1` AS grp,dsl.`col_2` AS name,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon
			FROM `'.$this->db->characterdb.'`.`character_skills` AS cs
			LEFT JOIN `dbc_skillline` as dsl ON (cs.`skill`=dsl.`col_0`)
			LEFT JOIN `dbc_spellicon` AS dsi ON (dsl.`col_4`=dsi.col_0)
			WHERE cs.guid=?',
			array($this->char['guid'])
		);
		$skills = $get_skills->fetchAll(PDO::FETCH_ASSOC);
		$this->store_cache(array('char_skills',$this->char['guid']),$skills);
		return $skills;
	}

	/**
	 * Gets character's talents (primary and secondary) and talents distribution
	 * @return array character's inventory
	 */
	public function get_char_talents() {
		if (!$this->char['guid'])
			return;

		if ($talents = $this->get_cache(array('char_talents',$this->char['guid']),CHAR_EXPIRE)) {
			return $talents;
		}
			
		$get_talent_tabs = $this->db->query('
			SELECT tt.`col_0` AS tree_id,tt.`col_5` AS tree_index,tt.`col_6` AS tree_name,REPLACE(LOWER(si.`col_1`),"interface\\\\icons\\\\","") AS icon
			FROM `dbc_talenttab` as tt
			LEFT JOIN `dbc_spellicon` as si ON (tt.`col_2` = si.`col_0`)
			WHERE tt.`col_3`= 1 << ?',
			array($this->char['class']-1)
		);
		$tabs = $get_talent_tabs->fetchAll(PDO::FETCH_ASSOC);

		$get_glyphs = $this->db->query('
			SELECT `spec`,`glyph1`,`glyph2`,`glyph3`,`glyph4`,`glyph5`,`glyph6`,`glyph7`,`glyph8`,`glyph9`
			FROM `'.$this->db->characterdb.'`.`character_glyphs`
			WHERE `guid`=?',
                	array($this->char['guid'])
		);
                $glyphs = array( 'primary' => array(), 'secondary' => array());
		foreach ($get_glyphs->fetchAll(PDO::FETCH_ASSOC) as $g) {
			if ($g['spec'] == 0) {
				$spec = 'primary';
			} else {
				$spec = 'secondary';
			}
			for ($i=1;$i<=9;++$i) {
				if ($g['glyph'.$i] > 0) {
					$glyph = new Glyph($this->db,$g['glyph'.$i]);
					$glyphs[$spec][] = $glyph->get_glyph();
				}
			}
		}
			
		$get_talents = $this->db->query('
			SELECT dt.`col_1` AS grp,dt.`col_2` AS tier,dt.`col_3` AS col,ct.spec,CASE ct.`spell` WHEN dt.`col_4` THEN "1" WHEN dt.`col_5` THEN "2" WHEN dt.`col_6` THEN "3" WHEN dt.`col_7` THEN "4" WHEN dt.`col_8` THEN "5" END AS points,ct.`spell`
			FROM `'.$this->db->characterdb.'`.`character_talent` AS ct 
			LEFT JOIN `dbc_talent` AS dt ON (ct.`spell` IN (dt.`col_4`,dt.`col_5`,dt.`col_6`,dt.`col_7`,dt.`col_8`))
			WHERE ct.`guid`=?',
			array($this->char['guid'])
		);
		$talents = array('tabs' => $tabs, 'talents' => $get_talents->fetchAll(PDO::FETCH_ASSOC), 'glyphs' => $glyphs);
		$this->store_cache(array('char_talents',$this->char['guid']),$talents);
		return $talents;

	}




	/**
	* Gets character's completed quests in zone
	* @return array character's inventory
	*/
	public function get_completed_quests($zone) {
		if (!$this->char['guid'])
			return;

		// add cache here

		$quests = array();
		$get_quests = $this->db->query('
			SELECT qt.`Id`
			FROM `'.$this->db->characterdb.'`.`character_queststatus_rewarded` AS cqr
			LEFT JOIN `'.$this->db->worlddb.'`.`quest_template` AS qt ON (qt.`Id`=cqr.`quest`)
			WHERE qt.`ZoneOrSort` = ? AND cqr.`guid` = ?',            // no limitation
			array($zone,$this->get_guid())
		);
		foreach ($get_quests->fetchAll(PDO::FETCH_ASSOC) as $q) {
			$quests[] = $q['Id'];
		}
		return $quests;
        }

	/**
	* Gets character's completed achievements in zone
	* @return array character's inventory
	*/
	public function get_completed_achievements($category) {
		if (!$this->char['guid'])
			return;

		// search for cached data. Set variable and stop processing when found.
		if ($achievements = $this->get_cache(array('char_achievements_in_category',$category),GENERAL_DBC_EXPIRE)) {
			return $achievements;
		}

		$achievements = array();
		$get_achievements = $this->db->query('
			SELECT ca.achievement,ca.date FROM `dbc_achievement` AS da
			LEFT JOIN `'.$this->db->characterdb.'`.`character_achievement` AS ca ON (da.`col_0`=ca.achievement)
			WHERE  da.`col_6` = ? AND  ca.`guid` = ?',		// no limitation
			array($category,$this->get_guid())
		);
		$achievements = $get_achievements->fetchAll(PDO::FETCH_ASSOC);
		$this->store_cache(array('char_achievements_in_category',$category),$achievements);

		return $achievements;
        }

	/**
	* Gets character's mailbox
	* @return array mails
	*/
	public function get_char_mailbox() {
		if (!$this->char['guid'])
			return;

		// only allow logged owners to see their mails
		if ($_SESSION['account'] != $this->char['account'])
			return;

		// search for cached data. Set variable and stop processing when found.
		if ($mails = $this->get_cache(array('char_mailbox',$this->get_guid()),CHAR_MAILBOX_EXPIRE)) {
			return $mails;
		}

		$mails = array();
		$get_mails = $this->db->query('
			SELECT m.`id`,m.`messageType`,m.`sender`,m.`subject`,m.`body`,m.`has_items`,m.`expire_time`,m.`deliver_time`,m.`money`,m.`cod`,m.`checked`,ch.`name`
			FROM `'.$this->db->characterdb.'`.`mail` AS m
			LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (m.`sender`=ch.`guid`)
			WHERE  m.`receiver` = ?',	// no limitation
			array($this->get_guid())
		);
		if ($get_mails->rowCount() != 0) {
			$mails = $get_mails->fetchAll(PDO::FETCH_ASSOC);
			foreach ($mails as &$m) {
				if ($m['has_items'] == 1) {
					$get_items = $this->db->query('
						SELECT item_guid
						FROM `'.$this->db->characterdb.'`.`mail_items` AS mi
						WHERE mi.mail_id=? AND mi.receiver=?',		// no limitation
						array($m['id'],$this->get_guid())
					);
					$m['items'] = array();
					foreach ($get_items->fetchAll(PDO::FETCH_ASSOC) as $j) {
						$item = new Item($this->db);
						$item->get_by_guid($j['item_guid']);
						$i = $item->get_item();
						$m['items'][] = array( 'guid' => $j['item_guid'], 'name' => $i['name'], 'itemEntry' => $i['itemEntry'], 'quality' => $i['quality'], 'count' => $i['count'], 'icon' => $i['icon'] );
					}
				}
			}
			$this->store_cache(array('char_mailbox',$this->get_guid()),$mails);
		}
		return $mails;
        }

	/**
	 * Gets character's auctions
	 * @return array character's auctions
	 */
	public function get_char_auctions() {
		if (!$this->char['guid'])
			return;

		// only allow logged owners to see their auctions
		if ($_SESSION['account'] != $this->char['account'])
			return;

		// search for cached data. Set variable and stop processing when found.
		if ($auctions = $this->get_cache(array('char_auctions',$this->get_guid()),CHAR_AUCTION_EXPIRE)) {
			return $auctions;
		}

		$auctions = array();
		$get_auctions = $this->db->query('
			SELECT a.`itemguid`,a.`buyoutprice`,a.`time`,a.`buyguid`,a.`lastbid`,a.`startbid`,a.`deposit`,ch.`name` AS buyerName
			FROM `'.$this->db->characterdb.'`.`auctionhouse` AS a
			LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (a.buyguid=ch.guid)
			WHERE a.`itemowner`=?',			// no limitation
			array($this->get_guid())
		);
		if ($get_auctions->rowCount() != 0) {
			$auctions = $get_auctions->fetchAll(PDO::FETCH_ASSOC);
			foreach ($auctions as &$a) {
				$item = new Item($this->db);
				$item->get_by_guid($a['itemguid']);
				$i = $item->get_item();

				$a['item'] = array('name' => $i['name'], 'itemEntry' => $i['itemEntry'], 'quality' => $i['quality'], 'count' => $i['count'], 'icon' => $i['icon'] );

			}
			$this->store_cache(array('char_auctions',$this->get_guid()),$auctions);
		}

		return $auctions;
	}

	/**
	 * Gets character's gearscore
	 * @return integer character's gearscore
	 */
	private function _get_char_gearscore() {
		$gearscore = 0;
		$titan = FALSE;
		$slotscore = array();

		foreach ($this->items as $i) {
			if ($i['bag'] == 0 && $i['slot'] <= 18) {
				$item = new Item($this->db);
				$item->get_by_entry($i['itemEntry']);
				$slotscore[$i['slot']] = $item->score;
				if ($i['slot'] == 16 && $i['inventoryType'] == 17)
					$titan = TRUE;
			}
		}

		foreach ($slotscore as $k => $v) {
			if (($k == 15 && $titan) || ($k == 16 && $titan)) {
				$gearscore += ($v / 2);
			} else {
				$gearscore += $v;
			}
		}
		return $gearscore;
	}



}
