<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Guild extends Cache {

	public $Armory;

	protected $_guild;
	protected $db;


	/**
	 * @param PDO database handler
	 * @param string|integer guid or name of guild
	 */
	function __construct($db,$armory) {
		$this->db = $db;
		$this->Armory = $armory;
	}


	public function get_by_guid($guid=0) {
		// search for cached data. Set variable and stop processing when found.
		if ($this->_guild = $this->get_cache(array('guild',$guid),GUILD_EXPIRE,$this->Armory)) {
			return;
		}

		if (intval($guid) != 0) {
			$get_guild = $this->db->query('
				SELECT chg.`guildid`,chg.`name` AS guildName,chg.`level` AS guildLevel,chg.`experience`,chg.`todayExperience`,ch.`name` AS leaderName,ch.`race` AS leaderRace
				FROM `'.$this->db->characterdb.'`.`guild` AS chg
				LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (chg.`leaderguid`=ch.`guid`)
				WHERE chg.`guildid`=?',
				array($guid)
			);
			if ($get_guild->rowCount() == 1) {
				$this->_guild = $get_guild->fetch(PDO::FETCH_ASSOC);
				$this->_process();
				$this->store_cache(array('guild',$guid),$this->_guild);
			}
		}
	}

        public function get_by_name($name) {
		// search for cached data. Set variable and stop processing when found.
		if ($this->_guild = $this->get_cache(array('guild',$name),GUILD_EXPIRE,$this->Armory)) {
			return;
		}

		if (is_string($name)) {
			$get_guild = $this->db->query('
				SELECT chg.`guildid`,chg.`name` AS guildName,chg.`level` AS guildLevel,chg.`experience`,chg.`todayExperience`,ch.`name` AS leaderName,ch.`race` AS leaderRace
				FROM `'.$this->db->characterdb.'`.`guild` AS chg
				LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (chg.`leaderguid`=ch.`guid`)
				WHERE chg.`name`=?',
				array($name)
			);
			if ($get_guild->rowCount() == 1) {
				$this->_guild = $get_guild->fetch(PDO::FETCH_ASSOC);
				$this->_process();
				$this->store_cache(array('guild',$guid),$this->_guild);
			}
		}
	}

	private function _process() {
                        $get_guild_ranks = $this->db->query('
				SELECT `rid`,`rname`,`rights`,`BankMoneyPerDay`
				FROM `'.$this->db->characterdb.'`.`guild_rank`
				WHERE `guildid`=?',
				array($this->_get_guild_id())
			);
			$this->_guild['ranks'] = $get_guild_ranks->fetchAll(PDO::FETCH_ASSOC);

                        $get_bank_rights = $this->db->query('
				SELECT `TabId`,`rid`,`gbright`
				FROM `'.$this->db->characterdb.'`.`guild_bank_right`
				WHERE `guildid`=?',
				array($this->_get_guild_id())
			);
			$rights = array();
			foreach ($get_bank_rights->fetchAll(PDO::FETCH_ASSOC) as $r) {
				$rights[$r['TabId']][$r['rid']] = $r['gbright'];
			}
			$this->_guild['rights'] = $rights;

			$get_applicants_finder =$this->db->query('
				SELECT `availability`,`classRoles`,`interests`,`level`,`listed`,`comment`
				FROM `'.$this->db->characterdb.'`.`guild_finder_guild_settings`
				WHERE `guildId`=?',
				array($this->_get_guild_id())
			);
			$this->_guild['recruitment'] = $get_applicants_finder->fetch(PDO::FETCH_ASSOC);

			$xp_levels = $this->get_xp_levels();
			$this->_guild['next_level_xp'] = $xp_levels[$this->_guild['guildLevel']];
error_log("this->_guild['next_level_xp']  = ".$this->_guild['next_level_xp'] );

	}

	/**
	 * Returns guild informations. Remove sensitive data before output
	 * @return array guild informations
	 */
	public function get_guild($noauth=FALSE) {
		if (!$this->_guild['guildid'])
			return;

		if ($noauth) {
			$rank = 0;
		} else {
			$rank = $this->Armory->auth->allow_guild_access($this->_guild['guildid']);
			// if rank is not 0 (false) - decrease rank (it was raised in Auth() as GuMa has rid=0 which is interpreted as FALSE
			if ($rank)
				--$rank;
		}

		// if rank is 0 (false) or rank is too low, remove sensitive data from array (tabs right, bank money limit, guild rights, ...)
		if (!$rank || $rank > 4) {
			foreach ($this->_guild['ranks'] as $key => $g) {
				unset($this->_guild['ranks'][$key]['rights']);
				unset($this->_guild['ranks'][$key]['BankMoneyPerDay']);
			}

			unset($this->_guild['rights']);
		}


		return $this->_guild;
	}

	/**
	 * Returns guild members. Remove sensitive data before output
	 * @return array guild members
	 */
	public function get_members($noauth=FALSE) {
		if (!$this->_guild['guildid'])
			return;

		$members = $this->_get_members();

		if ($noauth) {
			$rank = 0;
		} else {
			$rank = $this->Armory->auth->allow_guild_access($this->_guild['guildid']);
			if ($rank)
				--$rank;
		}

		if (!$rank) {
			foreach ($members as $key => $i) {
				unset($members[$key]['offnote']);
				unset($members[$key]['pnote']);
			}
		}

		return $members;
	}

	/**
	 * Returns guild bank tabs and items. Only to authenticated users and with appropriete permissions
	 * @return array Guild tabs and bank items
	 */
	public function get_guildbank() {
		if (!$this->_guild['guildid'])
			return;

		// to see at least basic guildbank, one character must be in the guild
		if ($rank = $this->Armory->auth->allow_guild_access($this->_guild['guildid'])) {
			// decrease rank (it was raised in Auth() as GuMa has rid=0 which is interpreted as FALSE
			--$rank;

			// get bank tabs and items
			$bank = $this->_get_bank();
			// loop for each items in guild bank
			foreach ($bank['items'] as $key => $i) {
				// check if item doesn't sits in tab to which user doesn't have permissions
				if (!array_key_exists($rank,$this->_guild['rights'][$i['TabId']]) || !$this->_guild['rights'][$i['TabId']][$rank]) {
					// if yes, remove item from array
					unset($bank['items'][$key]);
				}
			}
			return $bank;			
		}
		return FALSE;
	}

	/**
	 * Returns guild achievements in specified category - TODO
	 * @return false;
	 */
	public function achievements($category) {
		if (!$this->_guild['guildid'])
			return;

	}

	/**
	 * Returns guild guid
	 * @return integer guild guid
	 */
	private function _get_guild_id() {
		return $this->_guild['guildid'];
	}


	/**
	 * Private helper function for getting guild bank
	 * @return array Guild bank tabs and items
	 */
	private function _get_bank() {
                if ($bank = $this->get_cache(array('guild_bank',$this->_get_guild_id()),GUID_BANK_EXPIRE)) {
                        return $bank;
                }

		$get_guild_tabs = $this->db->query('
			SELECT `TabId`,`TabName`,`TabIcon`,`TabText`
			FROM `'.$this->db->characterdb.'`.`guild_bank_tab`
			WHERE guildid=?',
			array($this->_get_guild_id())
		);
		$tabs = $get_guild_tabs->fetchAll(PDO::FETCH_ASSOC);

		$get_bank = $this->db->query('
			SELECT gbi.`TabId`,gbi.`SlotId`,gbi.`item_guid` AS guid,ii.`itemEntry`,ii.`count`,dis.`col_99` AS name,dis.`col_1` AS quality,dis.`col_12` AS itemLevel,dis.`col_9` AS inventoryType,dis.col_23 AS ContainerSlots,LOWER(idi.`col_5`) AS icon
			FROM `'.$this->db->characterdb.'`.guild_bank_item AS gbi
			LEFT JOIN `'.$this->db->characterdb.'`.item_instance AS ii ON (gbi.`item_guid`=ii.`guid`)
			LEFT JOIN `db2_item_sparse` AS dis ON (ii.`itemEntry`=dis.`col_0`)
			LEFT JOIN `db2_item` AS di ON (ii.`itemEntry`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE gbi.`guildid` = ?',
			array($this->_get_guild_id())
		);
		$items = $get_bank->fetchAll(PDO::FETCH_ASSOC);

		$data = array('tabs' => $tabs, 'items' => $items);
		$this->store_cache(array('guild_bank',$this->_get_guild_id()),$data);
		return $data;
	}

	/**
	 * Private helper function for getting guild members
	 * @return array Guild members
	 */
	private function _get_members() {
		// search for cached data. Return variable and stop processing when found.
		if ($members = $this->get_cache(array('guild_members',$this->_get_guild_id()),GUILD_EXPIRE)) {
			return $members;
		}

		$get_guild_members = $this->db->query('
			SELECT chgm.`rank`,chgm.`pnote`,chgm.`offnote`,ch.`guid`,ch.`name`,ch.`race`,ch.`class`,ch.`level`
			FROM `'.$this->db->characterdb.'`.`guild_member` AS chgm
			LEFT JOIN `'.$this->db->characterdb.'`.`characters` AS ch ON (chgm.`guid`=ch.`guid`)
			WHERE chgm.`guildid`=?',
			array($this->_get_guild_id())
		);

		$members = $get_guild_members->fetchAll(PDO::FETCH_ASSOC);
		$this->store_cache(array('guild_members',$this->_get_guild_id()),$members);
		return $members;
	}

	/**
	 * Global function for getting required guild levelups
	 * @return array Guild levels
	 */
	public function get_xp_levels() {
		// search for cached data. Return variable and stop processing when found.
		if ($levels = $this->get_cache(array('guild_levels'),GENERAL_DBC_EXPIRE)) {
			return $levels;
		}

		$get_guild_levels = $this->db->query('
			SELECT lvl, xp_for_next_level
			FROM `'.$this->db->worlddb.'`.`guild_xp_for_level`',
			array()
		);

		$levels = array();
		foreach ($get_guild_levels->fetchAll(PDO::FETCH_ASSOC) as $l) {
			$levels[$l['lvl']] = $l['xp_for_next_level'];
		}
		$this->store_cache(array('guild_levels'),$levels);
		return $levels;
	}




}
