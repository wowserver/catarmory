<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Loot extends Cache {

	protected $_loot;
	protected $db;

	/**
	 * @param PDO database handler
	 * @param integer entry of loot
	 */
	function __construct($db,$type,$lootid) {
		$this->db = $db;
		$this->type = $type;

		// search for cached data. Set variable and stop processing when found.
		if ($this->_loot = $this->get_cache(array('loot',$type,$lootid),NPC_EXPIRE)) {
			return;
		}
		
		$get_loot = $this->db->query('
			SELECT clt.`item`,clt.`ChanceOrQuestChance`,clt.`lootmode`,clt.`groupid`,clt.`mincountOrRef`,clt.`maxcount`
			FROM `'.$this->db->worlddb.'`.`'.$type.'_loot_template` AS clt
			WHERE clt.`entry`=? LIMIT '.SQL_LIMIT,	// add limit?
			array($lootid)
		);

		$this->_loot = array();
		foreach ($get_loot->fetchAll(PDO::FETCH_ASSOC) as $l) {
			if ($l['mincountOrRef'] > 0) {
				$item = new Item($this->db);
				$item->get_by_entry($l['item']);
				$i = $item->get_item();
				$l['quality'] = $i['quality'];
				$l['itemLevel'] = $i['itemLevel'];
				$l['inventoryType'] = $i['inventoryType'];
				$l['name'] = $i['name'];
				$l['icon'] = $i['icon'];

				$this->_loot[] = $l;
			} else {
				$this->_process_reference(abs($l['mincountOrRef']),$l['maxcount']);
			}
		}
		$this->store_cache(array('loot',$type,$lootid),$this->_loot);
	}

	/**
	 * Returns loot informations
	 * @return array loot informations
	 */
	public function get_loot_table() {
#		if (!$this->_loot['entry'])
#			return;

		return $this->_loot;
	}

	private function _process_reference($reference,$max) {
		$get_loot = $this->db->query('
			SELECT rlt.`item`,rlt.`ChanceOrQuestChance`,rlt.`lootmode`,rlt.`groupid`,rlt.`mincountOrRef`,rlt.`maxcount`
			FROM `'.$this->db->worlddb.'`.`reference_loot_template` AS rlt
			WHERE rlt.`entry`=? LIMIT '.SQL_LIMIT,
			array($reference)
		);
		$count = $get_loot->rowCount();
		foreach ($get_loot->fetchAll(PDO::FETCH_ASSOC) as $l) {
			if ($l['mincountOrRef'] > 0) {
				$l['ChanceOrQuestChance'] = sprintf("%.2f",($max / $count)*100);
				$item = new Item($this->db);
				$item->get_by_entry($l['item']);
				$i = $item->get_item();
				$l['quality'] = $i['quality'];
				$l['itemLevel'] = $i['itemLevel'];
				$l['inventoryType'] = $i['inventoryType'];
				$l['name'] = $i['name'];
				$l['icon'] = $i['icon'];

				$this->_loot[] = $l;
			} else {
				$this->_process_reference(abs($l['mincountOrRef']),$l['maxcount']);
			}
		}
	}
	

}
