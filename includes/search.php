<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include_once "includes/items.php";
include_once "includes/guilds.php";
include_once "includes/arenas.php";
include_once "includes/quests.php";
include_once "includes/npcs.php";
include_once "includes/spells.php";
include_once "includes/achievements.php";

class Search {

	protected $db;
	protected $_string;

	/**
	 * Initialize new search
	 */
	function __construct($db) {
		$this->db = $db;
		$this->_string = '';
	}
	
	/**
	 * Search in characters
	 * @return array characters list
	 */
	private function _search_characters() {
		$get_chars = $this->db->query('
			SELECT `guid`,`name`,`race`,`class`,`level`
			FROM `'.$this->db->characterdb.'`.`characters`
			WHERE `name` COLLATE utf8_general_ci LIKE ? LIMIT '.SQL_LIMIT,		// consider adding fulltext over name filed in mysql and use AGAINST
			array('%'.$this->_string.'%')
		);

		return $get_chars->fetchAll(PDO::FETCH_ASSOC);
	}
	

	/**
	 * Perform search on multiple entities
	 * @param String search string
	 * return Array result array
	 */
	function results($string) {
		$this->_string = $string;

		$out = array();
		$out['characters'] = $this->_search_characters();

		$items = new Items($this->db);
		$out['items'] = $items->search_by_name($this->_string);

		$guilds = new Guilds($this->db);
		$out['guilds'] = $guilds->search_by_name($this->_string);

		$arenateams = new Arenateams($this->db);
		$out['arenateams'] = $arenateams->search_by_name($this->_string);

		$quests = new Quests($this->db);
		$out['quests'] = $quests->search_by_title($this->_string);

		$npcs = new Npcs($this->db);
		$out['npcs'] = $npcs->search_by_name($this->_string);

		$spells = new Spells($this->db);
		$out['spells'] = $spells->search_by_name($this->_string);

		$achievements = new Achievements($this->db);
		$out['achievements'] = $achievements->search_by_name($this->_string);

		return $out;
	}
}
