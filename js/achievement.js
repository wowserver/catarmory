/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Achievement class
 * @class Achievement
 * @singleton
 */
Achievement = (function(a) {
	var Achievement = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Achievement.prototype, {
		/**
		 * Initialize achievement
		 * @param {Object} n achievement object data from server
		 */
		init: function(a) {
			this.id = a.id
			this.name = a.name;
			this.icon = a.icon || 'trade_engineering';
			this.description = a.description;
			this.achievers = a.achievers;
		},

		/**
		* Init basic stuff - render
		*/
		initBasic: function() {
			
			$('#achievement_name').text(this.name);
			$('#achievement_icon').html('<img src="/icons/'+this.icon+'.png">');
			$('#achievement_description').text(this.description);

			var achieversList = new List(ListView.templates.achievers,this.achievers,{
				hideNumResults:true
			});
			
			achieversList.render($('#achievers_wrapper'));
		}
	});

	return Achievement;
})();
  