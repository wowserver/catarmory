/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Guild class
 * @class Guild
 * @singleton
 */
Guild = (function(g) {
	var Guild = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Guild.prototype, {
		/**
		 * Initialize Guild
		 * @param {Object} c Guild object data from server
		 */
		init: function(g) {
			this.guildId = g.guildid;
			this.name = g.guildName;
			this.faction = (1<<(g.leaderRace-1) & racemaskAlliance ? 1 : 0);
			this.level = g.guildLevel;
			this.xp = g.experience;
			this.todayXp = g.todayExperience;
			this.leader = g.leaderName;
			this.ranks = g.ranks;
		},

		/**
		 * Init basic stuff - render
		 */
		initBasic: function(activeTab) {
			
			this.tabs = new Tabs($('#ul_menu'),$('#guild_result'));

			// first tab is static (for future use of mapper)
			this.tabs.add({
				id: 'basic',
				name: L12N('basic'),
				$el: $('#guild_basic'),
				data: function() {
					// now, when function is passed as data, it knows that $el holds pre-generated tab content
					// this needs to be revisited - probably set data to null?
				},
				onClick: function(obj) {
					Router.setParameter(1,obj.id);
				},
				_scope: this
			});
			
			
			
			this.initTabs(activeTab);
			
			$('#flag').css('background','url(\'images/b_'+this.faction+'.jpg\')');
			$('#guild_name').html(this.name);
			$('#guild_level').text(this.level);
		},

		/**
		 * Return Guild guid
		 * @return {Number} guid
		 */
		getGuildId: function() {
			return this.guildId;
		},

	
		initTabs: function(activeTab) {
			var totalTabs = 0;
			for (var i in guildMenu) {
				++totalTabs;
				Main.fetch(
					{
						what: 'guild',
						action: guildMenu[i].name,
						guildid: this.getGuildId()
					}, function(response) {
						var data = response.data;
						
						var list;
						switch (guildMenu[i].name) {
							case 'members':
								list = new List(ListView.templates.members,data,{
									additionalData: {
										ranks: this.ranks
									}
								});
								break;
						}
						
						--totalTabs;
						
						this.tabs.add({
							id: guildMenu[i].name,
							name: L12N(guildMenu[i].name),
							data: list,
							onClick: function(obj) {
								Router.setParameter(1,obj.id);
							},
							_scope: this
						});

						if (totalTabs == 0) {
							this.tabs.render();
							this.tabs.activate(activeTab ? activeTab : 'basic');
						}
						
					},this
				);
			}

			

			// watch for hash changes of 1st index (activate appropriate tab)

			Router.bind(1,function(tab) {
				this.tabs.activate(tab);
			});
		}
	
	
	});

	return Guild;
})();








