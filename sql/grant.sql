-- create new user
CREATE USER 'catarmory'@'localhost' IDENTIFIED BY 'password';
-- grant 0 privileges at beginning
GRANT USAGE ON *.* TO 'catarmory'@'localhost';
-- grant all major privileges on catarmory table
GRANT SELECT, EXECUTE, SHOW VIEW, ALTER, CREATE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, INDEX, INSERT, REFERENCES, UPDATE, LOCK TABLES  ON `wowarmory`.* TO 'catarmory'@'localhost' WITH GRANT OPTION;
-- grant select privilege on account table columns (id,username and sha_pass_hash) on auth database
GRANT SELECT(`id`,`username`,`sha_pass_hash`)  ON TABLE `trinity\_auth`.`account` TO 'catarmory'@'localhost';
-- grant select in all tables and columns in characters database
GRANT SELECT  ON `trinity\_characters`.* TO 'catarmory'@'localhost';
-- grant select in all tables and columns in world database
GRANT SELECT  ON `trinity\_world`.* TO 'catarmory'@'localhost';
-- reload privileges settings
FLUSH PRIVILEGES;
